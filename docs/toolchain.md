# Toolchain

## Git

* https://try.github.io
* [Learn X in Y minutes: Git](https://learnxinyminutes.com/docs/git/)
* [Git Cheatsheet](https://www.atlassian.com/dam/jcr:8132028b-024f-4b6b-953e-e68fcce0c5fa/atlassian-git-cheatsheet.pdf)
* [Git Internals](https://github.com/pluralsight/git-internals-pdf/raw/master/drafts/peepcode-git.pdf)

## CMake

- [Learn X in Y minutes: CMake](https://learnxinyminutes.com/docs/cmake/)
- [Learn CMake's Scripting Language in 15 Minutes](https://preshing.com/20170522/learn-cmakes-scripting-language-in-15-minutes/)
- [LLVM CMake Primer](https://llvm.org/docs/CMakePrimer.html)
- [An Introduction to Modern CMake](https://cliutils.gitlab.io/modern-cmake/)
- [Effective CMake](https://www.youtube.com/watch?v=bsXLMQ6WgIk), [slides](https://github.com/boostcon/cppnow_presentations_2017/blob/master/05-19-2017_friday/effective_cmake__daniel_pfeifer__cppnow_05-19-2017.pdf)
- [CMake by Example](https://mirkokiefer.com/cmake-by-example-f95eb47d45b1)

## Линтеры

- [Clang-Format](https://clang.llvm.org/docs/ClangFormat.html)
- [Clang-Tidy](http://clang.llvm.org/extra/clang-tidy/)
