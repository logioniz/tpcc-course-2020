# Теория и практика concurrency

_"You don't have to be an engineer to be a racing driver, but you do have to have Mechanical Sympathy"_ – Jackie Stewart, racing driver

Добро пожаловать в репозиторий курса!

- Прочтите [Getting Started](/docs/getting-started.md) прежде чем клонировать этот репозиторий.

- [Как сдавать задачи](docs/ci.md)
- [Приложение](http://tpcc.btty.su:5111/)
- [Scoreboard](https://docs.google.com/spreadsheets/d/1nkKbVLGtXYFRLfYaie8s8k2-Rq2_jHvC-tqdik6iuWo/edit?usp=sharing)

## Навигация

* `docs` – инструкции, правила, стайлгайд, FAQ и другие полезные документы

* `tasks` – домашние задания

* `lectures` – план курса и расписание лекций, ссылки, статьи и т.п.

* `library` – библиотека для тестирования решений

* `client` – консольный клиент для работы с задачами

Остальные директории – служебные.
